﻿using System;
using Microsoft.Exchange.WebServices.Data;
using System.Collections.Generic;
using System.Threading.Tasks;
using log4net;
using log4net.Config;
[assembly: log4net.Config.XmlConfigurator]

class Program
{
    public static readonly ILog log = LogManager.GetLogger(typeof(Program));


    static string command = "";
    static int noOfThreads = 0;
    static void Main(string[] args)
    {
        try
        {
            bool run = true;

            // NetworkMessage nwMsg = new NetworkMessage();
            //nwMsg.recieve();
            string email;
            string password;
            FileManagement.getCredentials(out email, out password);

            if (string.IsNullOrEmpty(password) || string.IsNullOrEmpty(email))
            {
                Program.log.Error("Email or password is empty. Closing.");
            }
            else
            {
                Program.log.Info("Email found: " + email);
                EmailHandling.newMailThreadAsync(email, password);
            }

            while (run)
            {
                command = Console.ReadLine();
                if (command == "status")
                {
                    EmailHandling.verbose = true;
                    Console.WriteLine("----");
                    Console.WriteLine("RESPONSE: Outputting status. Type 'stop' and press enter to stop status view.");
                    Console.WriteLine("----");
                }
                else if (command == "stop")
                {
                    EmailHandling.verbose = false;
                    Console.WriteLine("----");
                    Console.WriteLine("RESPONSE: Stopping status output. Type 'status' and press enter to see status.");
                    Console.WriteLine("----");
                }
                else
                {
                    Console.WriteLine("----");
                    Console.WriteLine("ERROR: Unrecognized command.");
                    Console.WriteLine("----");
                }
            }
        }
        catch (System.Exception e)
        {
            Console.WriteLine("ERROR: Something went wrong creating threads: " + e.Message);
        }

    }


}


