﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

    static class FileManagement
    {
        public const string ap = "carl.llewellyn.dnfew/vP7p";
        const string credFile = "credentials.txt";
        static public void saveCredentials(string email, string encryptedPassword)
        {
            try
            {
                if (File.Exists(credFile))
                {
                    File.Delete(credFile);
                }
                var file = File.CreateText(credFile);
                file.WriteLine(email);
                file.WriteLine(encryptedPassword);
                file.Flush();
                file.Close();
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        static public void getCredentials(out string email, out string password)
        {
            try
            {
                email = File.ReadAllLines(credFile)[0];
                password = AESThenHMAC.SimpleDecryptWithPassword(File.ReadAllLines(credFile)[1], FileManagement.ap);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                email = null;
                password = null;
            }
        }
    }
