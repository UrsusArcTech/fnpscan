﻿using System;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
namespace FNP_SetCredentials
{
    class Program
    {
        

        static void Main(string[] args)
        {
            Console.WriteLine("Email address: ");
            var email = Console.ReadLine();
            Console.WriteLine("Password: ");
            var password = Console.ReadLine();

            FileManagement.saveCredentials(email, AESThenHMAC.SimpleEncryptWithPassword(password, FileManagement.ap));
        }
    }
}
