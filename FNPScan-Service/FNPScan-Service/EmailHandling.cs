﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Exchange.WebServices.Data;
using System.IO;

static class EmailHandling
{
    public static bool verbose = false;
    static int waitTime = 2 * 1000;

    public static async System.Threading.Tasks.Task newMailThreadAsync(string username, string password)
    {
        Program.log.Info("Creating thread..");
        await System.Threading.Tasks.Task.Run(() =>
        {
            bool run = true;
            while (true)
            {
                ExchangeService service;

                service = new ExchangeService(ExchangeVersion.Exchange2016)
                {
                    Credentials = new WebCredentials(username, password)
                };


                // This is the office365 webservice URL
                service.Url = new Uri("https://summer.gov.bc.ca/EWS/Exchange.asmx");
                // Prepare seperate class for writing email to the database
                SearchFilter searchFilter = new SearchFilter.IsGreaterThanOrEqualTo(ItemSchema.DateTimeReceived, DateTime.Today);
            //    FindItemsResults<Item> itemsFromToday = service.FindItems(WellKnownFolderName.Inbox, searchFilter, new ItemView(100));


                try
                {
                    foreach (EmailMessage email in service.FindItems(WellKnownFolderName.Inbox, searchFilter, new ItemView(100)))
                    {
                        if (email.From.ToString() == "DBA App Mgr <SMTP:applmgr@ldq9pebs01.bcliquor.com>" && email.Subject.Contains("finp1: Duplicated Concurrent Requests Found"))
                        {
                            email.Load();
                            var fnpEmailScan = new FNPEmailScan(email.Body);

                            EmailMessage message = new EmailMessage(service);

                            message.CcRecipients.Add("ITWORKMO@Victoria1.gov.bc.ca");
                            message.ToRecipients.Add("Thomas.Lee@bcldb.com");
                            message.ToRecipients.Add("smitha.george@bcldb.com");
                            message.ToRecipients.Add("tejashree.rajadhyaksha@bcldb.com");
                            message.ToRecipients.Add("reza.haddadi@bcldb.com");
                            message.ToRecipients.Add("robert.kok@bcldb.com");
                            
                            message.Subject = "fnp1: Duplicated Concurrent Requests Found For: " + DateTime.Today.Date;
                            message.Body = fnpEmailScan.getProgramsFound();
                            message.SendAndSaveCopy();

                            email.Move(WellKnownFolderName.DeletedItems);
                        }
                    }

                }
                catch (Exception e)
                {
                    Program.log.Error("An error has occured: " + e.Message);
                    try
                    {
                        if (!(e.InnerException is System.Net.WebException) && !(e.InnerException is System.IO.IOException) 
                        && !(e.InnerException is System.Net.Sockets.SocketException)) //if this is not just a connection issue
                        {
                            var em = new EmailMessage(service);
                            em.ToRecipients.Add("carl.llewellyn@bcldb.com");
                            em.Subject = "FINPScan" + NetworkMessage.version + " error";
                            em.Body = new MessageBody(BodyType.Text, "An error has occured in thread for: " 
                                + username + "\r\n\n Exception message: " 
                                + e.Message + "\r\n\n Stack trace: \r\n" + e.StackTrace + "\r\n\n Exception target site: " 
                                + e.TargetSite + "\r\n\n Inner exception:" + e.InnerException + "\r\n\n Exception:\r\n" + e);
                            em.Send();
                        }
                    }
                    catch (Exception a)
                    {
                        Program.log.Error("Cannot send error email from: ");
                    }
                }
                System.Threading.Thread.Sleep(waitTime);
            }
        });
    }

}

