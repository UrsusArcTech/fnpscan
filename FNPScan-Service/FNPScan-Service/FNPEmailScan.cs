﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

class FNPEmailScan
{
    String[] listOfPrograms = { "Autoinvoice Import Program", "Autoinvoice Master Program", "Automatic Clearing for Receipts", "Automatic Receipts Creation Program (API)", "Automatic Remittances Creation Program (API)", "Format Automatic Receipts", "Interface Trip Stop - SRS", "LDB AR Auto Invoice Container Charge", "LDB AR Autoinvoice Preprocessor", "LDB AR Create and Apply Prepaid Receipts", "LDB Auto Create Drop Shipment PO", "LDB Auto Receive Drop Shipment", "LDB Cannabis Pay on Receipt AutoInvoice Program", "LDB Create AP Credit memo from Claimable Adjustment", "LDB Create AP Credit/Debit memos from WOM-017 DC Adjustments", "LDB Create Credit Memo and Invoice Adjustment", "LDB Create Credit Memo for Drop Shipments Returns", "LDB Credit Memo to Invoice Matching", "LDB OTC Master Control Program", "LDB PAP Invoice Adjustments", "LDB PDR AP Clearing", "LDB PDR AP Create CVM Invoices", "LDB PDR AP Master Program", "LDB PDR Cost of Sales and Returns Notification", "LDB PDR Process Settlements", "LDB WOM Pay On Receipt AutoInvoice", "Order Import", "Payables Open Interface Import", "Purchase Release", "Requisition Import", "CNB Cannabis B2B On Account Receipt Interface" };
    List<string> programsFound = new List<string>();

    public FNPEmailScan(string emailBody)
    {
        extractListFromEmail(Regex.Split(emailBody, "\r\n|\r|\n"));
    }

    void extractListFromEmail(string[] emailBody)
    {
        bool headerFound = false;
        bool headerLineFound = false;
        foreach (string line in emailBody)
        {
            if (line == "==============================================================================================================================================================")
            {
                break;
            }
            if (!headerFound && line.Contains("Request ID"))
            {
                headerFound = true;
            }
            else if (!headerLineFound && headerFound && line.Contains("=============================================================================================================================================================="))
            {
                headerLineFound = true;
            }
            else if (headerLineFound && headerFound)
            {
                tryAddApp(line, Regex.Match(line, "\"([^\"]*)\"").ToString());
            }

        }
    }

    void tryAddApp(string fullLine, string app)
    {
        Console.WriteLine(app);
        foreach (string program in listOfPrograms)
        {
            if (app == "\"" + program + "\"")
            {
                Console.WriteLine("Found: " + app);
                programsFound.Add(fullLine + " " + program);
            }
        }
    }



    public string getProgramsFound()
    {
        if (programsFound.Count == 0)
        {
            return "No duplicates found for: " + DateTime.Today.Date;
        }
        else
        {
            return programsFound.Count + " duplicates found for " + " " + DateTime.Today.Date + "<br>" 
                + String.Join("<br>", programsFound);
        }

    }
}

