﻿using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Diagnostics;

class NetworkMessage
{
    static int port;
    string message = Environment.MachineName;
    List<ExceptionNetwork> exceptions = new List<ExceptionNetwork>();
    public const string version = "v0.1b";

    public NetworkMessage()
    {
        setServer();
    }

    public void addException(ExceptionNetwork exc)
    {
        try
        {
            if (exc != null)
            {
                if (exceptions.Count <= 10)
                    exceptions.Add(exc);
            }
        }
        catch (Exception e)
        {
            Program.log.Error("Could not add network exception: " + e.Message);
        }
    }

    private void clearExceptions()
    {
        exceptions.Clear();
    }

    string selectResponse(NetworkStream server, string input)
    {
        string response = "No Response.";
        switch (input)
        {
            case "-c":
                clearExceptions();
                response = "Exceptions cleared.";
                break;
            case "-u":
                response = "Updating...";
                server.Write(System.Text.Encoding.ASCII.GetBytes(response), 0, response.Length);
                //call updater
                //Process.Start(@"\\hopfileshare02\hodasd\Temporary\carl\TEmail2016-Updater\TEmail2016-Updater.exe");
                //close self
                Environment.Exit(1);
                break;
            case "-a":
                response = "Y";
                break;
            case "-v":
                response = version;
                break;
            case "-hn":
                response = Environment.UserName;
                break;
            default:
                response = "Unknown input.";
                break;
        }

        //send exceptions, regardless of input
        if (exceptions.Count > 0)
            response += " " + exceptions[1].getMessage();

        return response;
    }

    bool update()
    {
        return false;
    }

    private void setServer()
    {
        try
        {
            var lines = File.ReadAllLines(Directory.GetCurrentDirectory() + "\\" + "socket.txt");
            port = int.Parse(lines[0]);
        }
        catch (Exception e)
        {
            Program.log.Error("Can't set socket.");
        }
    }

    public async System.Threading.Tasks.Task recieve()
    {

        TcpListener server = null;
        try
        {


            // TcpListener server = new TcpListener(port);
            server = new TcpListener(IPAddress.Any, port);

            // Start listening for client requests.
            server.Start();

            // Buffer for reading data
            Byte[] bytes = new Byte[256];
            String data = null;

            // Enter the listening loop.
            while (true)
            {
                Program.log.Info("Waiting for a connection...");

                // Perform a blocking call to accept requests.
                // You could also use server.AcceptSocket() here.
                TcpClient client = await server.AcceptTcpClientAsync();
                Program.log.Info("Conncected.");

                data = null;

                // Get a stream object for reading and writing
                NetworkStream stream = client.GetStream();

                int i;

                // Loop to receive all the data sent by the client.
                while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                {
                    // Translate data bytes to a ASCII string.
                    data = System.Text.Encoding.ASCII.GetString(bytes, 0, i);
                    Program.log.Info("Received: " + data);
                    // Process the data sent by the client.
                    //data = data.ToUpper();


                    //get message with a response, input is the data sent
                    byte[] msg = System.Text.Encoding.ASCII.GetBytes(selectResponse(stream, data));

                    // Send back a response.
                    stream.Write(msg, 0, msg.Length);
                    Program.log.Info("Sent: " + data);
                }

                // Shutdown and end connection
                client.Close();
            }
        }
        catch (SystemException e)
        {
            Program.log.Error("SocketException: ", e);
        }
        finally
        {
            // Stop listening for new clients.
            server.Stop();
            recieve();
        }
    }




}

