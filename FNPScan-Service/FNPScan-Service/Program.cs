﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using log4net;
using log4net.Config;
[assembly: log4net.Config.XmlConfigurator]

static class Program
{
    public static readonly ILog log = LogManager.GetLogger(typeof(Program));
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    static void Main()
    {

        System.IO.Directory.SetCurrentDirectory(System.AppDomain.CurrentDomain.BaseDirectory);
        ServiceBase[] ServicesToRun;
        ServicesToRun = new ServiceBase[]
        {
                new Service1()
        };
        ServiceBase.Run(ServicesToRun);
    }


}


