﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

public partial class Service1 : ServiceBase
{
    public Service1()
    {
        InitializeComponent();

    }

    protected override void OnStart(string[] args)
    {
        Program.log.Info("Service started.");
        setupThreads();
    }

    protected override void OnStop()
    {
        Program.log.Info("Service stopped.");
    }
    static void setupThreads()
    {
        try
        {
            bool run = true;

            Program.log.Info("Starting FINPScan " + NetworkMessage.version);
            string email;
            string password;
            FileManagement.getCredentials(out email, out password);
            
            if (string.IsNullOrEmpty(password) || string.IsNullOrEmpty(email))
            {
                Program.log.Error("Email or password is empty. Closing.");
            }
            else
            {
                Program.log.Info("Email found: " + email);
                EmailHandling.newMailThreadAsync(email, password);
            }
        }
        catch (System.Exception e)
        {
            Program.log.Error("Something went wrong creating threads: " + e.Message);
        }
    }
}
